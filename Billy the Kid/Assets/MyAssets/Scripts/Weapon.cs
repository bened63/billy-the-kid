using System;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum WeaponType
    {
        Bow, Revolver, Shotgun
    }

    [Header("Settings")]
    [SerializeField] private WeaponType _weaponType;
    [SerializeField] private float _projectileDamage = 10;
    [SerializeField] private float _fireRate = 1; // shots per second

    [Header("References")]
    [SerializeField] private ParticleSystem _hitFX;

    private Camera _main;
    private ParticleSystem _ps;
    private List<ParticleCollisionEvent> _collisionEvents;
    private float _timeLastShot;
    
    public bool canFire { get; set; }
    public WeaponType weaponType { get => _weaponType; }

    // Start is called before the first frame update
    private void Start()
    {
        Init();
    }

    public void Init()
    {
        _main = Camera.main;
        _ps = GetComponent<ParticleSystem>();
        _collisionEvents = new List<ParticleCollisionEvent>();
    }

    // Update is called once per frame
    private void Update()
    {
        _timeLastShot += Time.deltaTime;
        if (_timeLastShot > (1f/_fireRate))
        {
            canFire = true;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = _ps.GetCollisionEvents(other, _collisionEvents);

        for (int i = 0; i < numCollisionEvents; i++)
        {
            Vector3 pos = _collisionEvents[i].intersection;
            GameObject go = Instantiate(_hitFX, pos, Quaternion.identity).gameObject;
            Destroy(go, 0.5f);
        }

        if (other.CompareTag("Enemy"))
        {
            Enemy e = other.GetComponent<Enemy>();
            e.Hit(_projectileDamage);
        }
    }

    public void ShootAtDirection(Vector2 direction)
    {
        _timeLastShot = 0f;
        canFire = false;

        if (_main == null)
        {
            _main = Camera.main;
        }

        // Direction
        //Vector2 dir = _main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        //dir = dir.normalized;
        direction = direction.normalized;

        // Any parameters we assign in emitParams will override the current system's when we call Emit.
        // Here we will override the start color and size.
        var emitParams = new ParticleSystem.EmitParams();
        emitParams.velocity = direction * _ps.main.startSpeed.constant;

        switch (_weaponType)
        {
            case WeaponType.Bow:
                _ps.Emit(emitParams, 1);
                break;

            case WeaponType.Revolver:
                _ps.Emit(emitParams, 1);
                break;

            case WeaponType.Shotgun:

                // Rotate ParticleSystem to target direction
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; // Get angle from direction vector
                _ps.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward); // LookRotation does not work in 2D

                // Set shape of ParticleSystem
                Vector3 rotation = _ps.shape.rotation;
                rotation.z = _ps.shape.arc * -0.5f;
                var sh = _ps.shape;
                sh.rotation = rotation;

                // Play
                _ps.Play();
                break;

            default:
                break;
        }
    }
}