using UnityEngine;
using UnityEngine.Tilemaps;

public class MyTilemap : MonoBehaviour
{
    public Tile highlightTile;
    private Tilemap _tilemap;

    // Start is called before the first frame update
    private void Start()
    {
        _tilemap = GetComponent<Tilemap>();
        _tilemap.SetTile(_tilemap.WorldToCell(new Vector3(4.5f, 0, 0)), highlightTile);
    }

    // Update is called once per frame
    private void Update()
    {
    }
}