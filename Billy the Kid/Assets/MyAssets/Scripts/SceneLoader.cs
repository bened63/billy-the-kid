using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Animator _transition;
    [SerializeField] private float _transitionTime = 1f;

    // Update is called once per frame
    private void Update()
    {
    }

    public void LoadNextRoom()
    {
        StartCoroutine(IELoadNextRoom(1));
    }

    private IEnumerator IELoadNextRoom(int roomIndex)
    {
        _transition.SetTrigger("Start");
        yield return new WaitForSeconds(_transitionTime);

        // Set the current Scene to be able to unload it later
        Scene currentScene = SceneManager.GetActiveScene();

        // The Application loads the Scene in the background at the same time as the current Scene.
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(roomIndex, LoadSceneMode.Additive);

        // Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        // Move Player-GameObject to the newly loaded Scene
        GameObject goPlayer = GameObject.Find("Player");
        SceneManager.MoveGameObjectToScene(goPlayer, SceneManager.GetSceneAt(roomIndex));
        goPlayer.GetComponent<Player>().Init();
        goPlayer.transform.position = Vector3.zero;

        // Unload the previous Scene
        SceneManager.UnloadSceneAsync(currentScene);
    }
}