using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Player _player;
    [SerializeField] float _hitPoints = 10;
    [SerializeField] float _moveSpeed = 1f;
    [SerializeField] ParticleSystem _dieFX;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        if (_player == null)
        {
            Debug.Log("<color=lime>ERROR: No Player found!</color>");
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 dir = _player.transform.position - transform.position;
        dir = dir.normalized;
        transform.Translate(dir * Time.deltaTime * _moveSpeed * 0.1f);
    }

    public void Hit(float damage)
    {
        _hitPoints = Mathf.Clamp(_hitPoints - damage, 0, 100000);
        if (_hitPoints <= 0)
        {
            GameObject go = Instantiate(_dieFX, transform.position, Quaternion.identity).gameObject;
            Destroy(go, 0.7f);
            Destroy(gameObject);
        }
    }
}
