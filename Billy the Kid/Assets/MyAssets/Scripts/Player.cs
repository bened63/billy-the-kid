using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private WeaponHolder _weaponHolder;
    [SerializeField] private Animator _animator;

    [Header("Settings")]
    [SerializeField] private float _moveSpeed = 5;

    [SerializeField] private Weapon.WeaponType _equippedWeapon;

    private Vector3 _movement;
    [SerializeField] private bool _isShooting = false;

    // Start is called before the first frame update
    private void Start()
    {
        Init();
    }

    public void Init()
    {
        Debug.LogFormat("<color=lime>Init Player</color>");
        _weaponHolder.Init();
        _weaponHolder.ChangeWeapon(_equippedWeapon);
    }

    // Update is called once per frame
    private void Update()
    {
        CalculateMovement();

        if (Input.GetKeyDown(KeyCode.Space) && _weaponHolder.canFire)
        {
            // Play animation
            _isShooting = true;
            _animator.SetTrigger("Shoot");
            Shoot();
            StartCoroutine(IEResetIsShooting());
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    private IEnumerator IEResetIsShooting()
	{
        yield return new WaitForSeconds(1);
        _isShooting = false;
	}

    private void Shoot()
    {
        Vector2 dir = _movement;
        if (dir.x == 0 && dir.y == 0) dir = new Vector2(1, 0);
        _weaponHolder.FireWeapon(dir);
    }

    private void CalculateMovement()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        _movement = new Vector3(horizontalInput, verticalInput, 0).normalized;

        // Animations
        if (!_isShooting)
		{
            _animator.SetFloat("Horizontal", horizontalInput);
            _animator.SetFloat("Vertical", verticalInput);
            _animator.SetFloat("Speed", _movement.sqrMagnitude);
		}
    }

    private void Move()
    {
        transform.Translate(_movement * _moveSpeed * Time.deltaTime);
    }
}