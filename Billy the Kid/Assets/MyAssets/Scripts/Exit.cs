using UnityEngine;

public class Exit : MonoBehaviour
{
    private SceneLoader _sceneLoader;

    // Start is called before the first frame update
    private void Start()
    {
        _sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
        if (_sceneLoader == null)
        {
            Debug.Log("<color=lime>ERROR: No SceneLoader found!</color>");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.LogFormat("<color=lime>Exit</color>");

        if (collision.CompareTag("Player"))
        {

            _sceneLoader.LoadNextRoom();
        }
    }
}