using System;
using UnityEngine;

public class WeaponHolder : MonoBehaviour
{
    [SerializeField] private Weapon[] _weapons;
    private Weapon _equippedWeapon;

    public bool canFire => _equippedWeapon.canFire;

    public void Init()
    {
        foreach (Weapon w in _weapons)
        {
            w.Init();
        }
    }

    public void ChangeWeapon(Weapon.WeaponType weaponType)
    {
        foreach (Weapon w in _weapons)
        {
            if (w.weaponType == weaponType) _equippedWeapon = w;
        }
    }

    public void FireWeapon(Vector2 direction)
    {
        _equippedWeapon.ShootAtDirection(direction);
    }
}